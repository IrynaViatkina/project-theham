const tabsService = document.getElementsByClassName('btn-service');
const tabsPhoto = document.getElementsByClassName('btn-photo-work');
const divService = document.getElementsByClassName('service-part');
const photos = document.getElementsByClassName('photo-work');
const photosBlock = document.getElementsByClassName('photo-work-with-hidden-block');

const buttonLoadWork = document.getElementById("load-more-work");
const buttonLoadMasonry = document.getElementById("load-more-masonry");

const arrNewPhoto = [];
createArrayPhoto();
const arrNewMasonry = createArrayMasonry('masonry', 'masonry-add', 24);


document.addEventListener('click', function(event){
	if(event.target.className === 'btn-service'){
		activateTabService(event);
	}
	else if(event.target.className === 'btn-photo-work'){
		activateTabPhoto(event);
	}
	else if(event.target.id === 'load-more-work'){
		buttonLoadWork.value++;
		if(buttonLoadWork.value == 2){
			buttonLoadWork.remove();
		}
		document.getElementById('hide-loader-work').hidden = false;
		buttonLoadWork.hidden = true;
		setTimeout(function(){
			let arr = arrNewPhoto.splice(0, 12);
			document.getElementById("photo-collection").appendChild(createFragment(arr));
			sortPhotos(getActiveButtonPhoto());
			document.getElementById('hide-loader-work').hidden = true;
			buttonLoadWork.hidden = false;
		}, 2000);
	}
	else if(event.target.id === 'load-more-masonry'){
		buttonLoadMasonry.remove();
		document.getElementById('hide-loader-masonry').hidden = false;
		setTimeout(function(){
			let arr = arrNewMasonry.splice(0, 12);
			document.getElementById("masonry-container").appendChild(createFragment(arr));
			msnry.appended(arr);
			document.getElementById('hide-loader-masonry').remove();
		}, 2000);		
	}
});


function activateTabService(event){
	for(let i = 0; i < tabsService.length; i++){
		if (event.target === tabsService[i]){
			tabsService[i].classList.add('btn-service-active');
			divService[i].hidden = false;
		}
		else{
			tabsService[i].classList.remove('btn-service-active');
			divService[i].hidden = true;
		}
	}
}
function activateTabPhoto(event){
	for(let i = 0; i < tabsPhoto.length; i++){
		if (event.target === tabsPhoto[i]){
			tabsPhoto[i].classList.add('btn-photo-work-active');
			sortPhotos(event.target.id);
		}
		else{
			tabsPhoto[i].classList.remove('btn-photo-work-active');
		}
	}
}

function createArrayPhoto(){
	let arrGraphic = createNodePhoto('btn-graphic', 'graphic design', 7);
	let arrWeb = createNodePhoto('btn-web', 'web design', 7);
	let arrLanding = createNodePhoto('btn-landing', 'landing pages', 7);
	let arrWordpress = createNodePhoto('btn-wordpress', 'wordpress', 7);
	
	Array.prototype.push.apply(arrNewPhoto, arrGraphic);
	Array.prototype.push.apply(arrNewPhoto, arrWeb);
	Array.prototype.push.apply(arrNewPhoto, arrLanding);
	Array.prototype.push.apply(arrNewPhoto, arrWordpress);
	arrNewPhoto.sort(function(a, b){
		return Math.random() - 0.5;
	});	
}

function createNodePhoto(dataValue, imgFolder, amount){
	let elem = document.getElementsByClassName("photo-work-with-hidden-block")[0];
	let arr = [];
	for(let i = 1; i <= amount; i++){
		let clone = elem.cloneNode(true);
		let imgSrc = `img/${imgFolder}/${i}.jpg`;
		
		clone.querySelector('img').setAttribute('data-btn', dataValue);
		clone.querySelector('img').setAttribute('src', imgSrc);
		clone.querySelector('img').setAttribute('alt', imgFolder);
		clone.querySelector('.design-type').innerHTML = imgFolder;
		arr.push(clone);
	}
	return arr;
}

function createArrayMasonry(dataValue, imgFolder, amount){
	let elem = document.getElementsByClassName("masonry-block")[0];
	let arr = [];
	for(let i = 1; i <= amount; i++){
		let clone = elem.cloneNode(true);
		let imgSrc = `img/${imgFolder}/${i}.jpg`;
		clone.querySelector('img').setAttribute('data-btn', dataValue);
		clone.querySelector('img').setAttribute('src', imgSrc);
		clone.querySelector('img').setAttribute('alt', dataValue);
		arr.push(clone);
	}
	arr.sort(function(a, b){
		return Math.random() - 0.5;
	});	
	return arr;
}
function createFragment(arr){
	let fragment = document.createDocumentFragment();
	for(let i = 0; i < arr.length; i++){
		fragment.appendChild(arr[i]);
	}
	return fragment;
}

function getActiveButtonPhoto(){
	for(let i = 0; i < tabsPhoto.length; i++){
		if (tabsPhoto[i].classList[1] === 'btn-photo-work-active'){
			return tabsPhoto[i].id;
		}
	}
}
function sortPhotos(id){
	if(id === 'btn-all'){
		for(let i = 0; i < photos.length; i++){
			photosBlock[i].hidden = false;
		}
	}
	else{
		for(let i = 0; i < photos.length; i++){
			if(photos[i].getAttribute('data-btn') === id){
				photosBlock[i].hidden = false;
			}
			else{
				photosBlock[i].hidden = true;
			}
		}
	}
}